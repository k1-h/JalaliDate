# HOW-TO GUIDE 

### Convert Gregorian calendar date to Jalali

```
$date = new Jalali();
echo $date->setGregorianDate(new \DateTime())->getJalali()->format('Y/m/d');
```

As you may have noticed, you can use [PHP's standard date format characters](http://php.net/manual/en/function.date.php) to format your Jalali date. Isn't that cool?

### Convert Jalali calendar date to Gregorian

```
$date = new Jalali(1392, 6, 6);
echo $date->getGregorian()->format('Y/m/d');
```

### Formatting the converted date

Let's say you want to output the Jalali converted date to something like "پنج شنبه، ۸ شهریور ۱۳۹۲"
You can achieve this by 

```
$date = new Jalali();
echo $date->setGregorianDate(new \DateTime())->getJalali()->format('l, d F Y');
```

This looks pretty much like the above code. All I have done is that I have provided a different format string ```l, d F Y```

### Adding or subtracting units of date/time to/from the Jalali calendar date
To add or subtract a number of years/months/days/hours/minutes/seconds to the converted Jalali date you can use the ```add()``` and ```subtract()``` method.

This method accepts [PHP's standard DateInterval characters](http://www.php.net/manual/en/dateinterval.construct.php) as arguments.

Let's say you want to add 7 days to the current date.

```
$date = new Jalali(1392, 6, 6);
echo $date->add('P7D')->format('Y/m/d');
```

For subtracting 7 days you could do it like this:

```
$date = new Jalali(1392, 6, 6);
echo $date->sub('P1W')->format('Y/m/d');
```

### Convert TIMESTAMP to a Jalali calendar date
```
$date = new Jalali();
echo $date->setTimestamp(time())->getJalali()->format('Y/m/d');
```

It was that simple. All you need to do to convert a timestamp.
